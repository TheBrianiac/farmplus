# README #

This is a sample of my understanding of Java and the Bukkit API.

## PURPOSE ##

This plugin is an improvement to default Minecraft farming. Players must use hoes to harvest crops, and they receive different amounts of drops based on the tool tier. Fringe events are accounted for, preventing work-arounds/exploits. This plugin was originally designed to allow for LogBlock/CoreProtect logging - this is accomplished by not cancelling 'successful' events (i.e. when a crop is broken with a hoe).

## CONCEPTS ##

The code displays object-oriented programming, package organization, Maven implementation, and 1.9 materials/blocks.

## FUNCTIONALITY ##

###ADVANCED FARMING###

- Punching crops (beets, carrots, wheat, netherwarts) will not drop items.

- Harvesting crops with a hoe will drop a number of items (1 with wood/stone, 2 with iron/gold, 3 with diamond).

- Hoes enchanted with fortune will drop an extra 1 item per fortune level.

- Hoes used to harvest will lose durability (factors in unbreaking enchantment).

###ANTI-EXPLOIT###
Fringe events are accounted for.

- Crops on soil broken by players will not drop items.

- Crops on soil moved by pistons will not drop items.

- Crops destroyed by moving pistons will not drop items.

- Crops trampled by entitites will not drop items.

- Crops destroyed by water (and technically lava) flow will not drop items.

- Crops destroyed by explosions will not drop items.

###RANDOM DROPS###

- When a golden hoe is used to harvest carrots, there's a 5% chance to drop a golden carrot.

###COMMANDS###
- /farmplus will display the plugin version.