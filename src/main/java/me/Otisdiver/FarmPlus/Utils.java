package me.Otisdiver.FarmPlus;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.CropState;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.material.Crops;
import org.bukkit.material.MaterialData;

/** Contains methods to be used a lot. **/
public class Utils {
	/** Utils contains methods to be used a lot. **/
	public Utils() {
		defineCrops();
		defineTools();
	}
	
	// Code pertaining to the crops.
	private final List<Material> crops = new ArrayList<Material>();
	
	/** Adds needed elements to List<Material> crops **/
	private void defineCrops() {
		crops.add(Material.BEETROOT_BLOCK);
		crops.add(Material.CARROT);
		crops.add(Material.CROPS);
		crops.add(Material.NETHER_STALK);
	}
	
	/** Returns the list of materials to be managed. **/
	public List<Material> getCrops() {
		return this.crops;
	}
	
	/* Crop Name	Material Value		Fully Grown DV
	 * Beets 		BEETROOT_BLOCK	 	3
	 * Carrots 		CARROT 				7
	 * Wheat 		CROPS 				7
	 * Netherwart 	NETHER_STALK 		3
	 */
	
	/** Checks whether a material is in the list of crops to manage.
	 * 
	 * @param type The material type to check.
	 * @return Returns true if the material should be managed. */
	public boolean isManagedCrop(Material type) {
		return getCrops().contains(type);
	}
	
	/** Checks whether a crop is fully grown.
	 * 
	 * @param block The block to check.
	 * @return Returns 'true' if the crop is fully grown. */
	public boolean isFullyGrown(Block block) {
		MaterialData md = block.getState().getData();
		
		if(md instanceof Crops) {
			return (((Crops) md).getState() == CropState.RIPE);
		} else
			return false;
	}
	
	/** Deletes the given block if necessary.
	 * 
	 * @param block The block to check for deletion. */
	public void checkBlock(Block block) {
		// find the type of block being flowed to, remember it
		Material type = block.getType();
		
		// if we care about this type of block
		if(getCrops().contains(type)) {
			block.setType(Material.AIR);
		}
	}
	
	/** Essentially runs checkBlock(block) on the block above the given block.
	 * 
	 * @param block The block below the block to be checked. */
	public void checkSoil(Block block) {
		// if it's soil (plowed dirt)
		if(block.getType().equals(Material.SOIL)) {
			// get the location 1 above the block
			Location aboveloc = block.getLocation().add(0, 1, 0);
			
			// find the block at this location
			Block above = aboveloc.getBlock();
			
			// delete the block above if necessary
			checkBlock(above);
		}
	}
	
	// Code pertaining to the tools.
	private final List<Material> tools = new ArrayList<Material>();
	
	/** Adds needed elements to List<Material> tools **/
	private void defineTools() {
		tools.add(Material.WOOD_HOE);
		tools.add(Material.STONE_HOE);
		tools.add(Material.IRON_HOE);
		tools.add(Material.GOLD_HOE);
		tools.add(Material.DIAMOND_HOE);
	}
	
	/** Returns the list of tools that can be used to harvest. **/
	public List<Material> getTools() {
		return this.tools;
	}
	
	Random random = new Random();
	
	public boolean chance(int percent) {
	    return random.nextInt(100) < percent;
	}
}