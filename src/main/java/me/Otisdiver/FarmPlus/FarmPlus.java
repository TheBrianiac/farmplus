package me.Otisdiver.FarmPlus;

import org.bukkit.plugin.java.JavaPlugin;

import me.Otisdiver.FarmPlus.Listeners.FarmerListener;
import me.Otisdiver.FarmPlus.Listeners.MiscListener;
import me.Otisdiver.FarmPlus.Listeners.PistonListener;

/** Main plugin class. Extends JavaPlugin. **/
public class FarmPlus extends JavaPlugin {
	
	private Utils utils;
	
	// Method called when plugin is loaded.
	public void onEnable() {
		// Find the plugin's version and remember it.
		String version = this.getDescription().getVersion();
		
		// Register /farmplus command.
		this.getCommand("farmplus").setExecutor(new Commander(version));
		
		// Initialize the utilities class.
		utils = new Utils();
		
		// Register event listeners.
		getServer().getPluginManager().registerEvents(new FarmerListener(utils), this);
		getServer().getPluginManager().registerEvents(new PistonListener(utils), this);
		getServer().getPluginManager().registerEvents(new MiscListener(utils), this);
		
		// Log success.
		this.getLogger().info("FarmPlus version " + version + " loaded successfully.");
	}
	
	// Method called when plugin us unloaded.
	public void onDisable() {
		// Log success.
		this.getLogger().info("FarmPlus unloaded successfully.");
	}
}
