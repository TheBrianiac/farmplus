package me.Otisdiver.FarmPlus.Listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.ChatColor;

import me.Otisdiver.FarmPlus.Utils;

/** Processes player block break events. **/
public class FarmerListener implements Listener {
	
	// instance of utilities class
	private Utils		utils;
	
	/** FarmerListener processes piston extension and retraction events.
	 * 
	 * @param utils Instance of the Utilities class initialized in the main class. */
	public FarmerListener(Utils utils) {
	    this.utils = utils;
	}
	
	// Code runs when a block is broken.
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled=true) // WorldGuard monitors Normal
	public void onBreak(BlockBreakEvent e) {
		// if we don't care about this block
		if(!utils.isManagedCrop(e.getBlock().getType())) {
			// delete any crops planted on it if it's soil
		    utils.checkSoil(e.getBlock());
			
			// don't go further
			return;
		}
		
		// determine if crop is fully grown
		boolean grown = utils.isFullyGrown(e.getBlock());
		
		// save the block type before deleting it
		Material type = e.getBlock().getType();
		// delete the block
		e.getBlock().setType(Material.AIR);
		
		// find the tool in the user's hand and remember it
		ItemStack toolitem = e.getPlayer().getInventory().getItemInMainHand();
		Material tool = toolitem.getType();
		
		// don't go further if the crop isn't grown (still needed to break it)
		if(!grown) {
			// if they're holding a hoe, apply damage for breaking the unripe crop
			if(utils.getTools().contains(tool)) damageToolInHand(e.getPlayer());
			return;
		}
		
		// if the player isn't using a proper tool
		if(!utils.getTools().contains(tool)) {
			// don't go further
			return;
		}
		
		// calculate the number of items to drop
		int numDrops = getDropNumber(toolitem, tool);
		
		// get the block's location and remember it
		Location loc = e.getBlock().getLocation();
		
		// 5% chance of golden carrot if golden hoe used
		if(tool.equals(Material.GOLD_HOE) && utils.chance(5)) {
			ItemStack goldcarrot;
			goldcarrot = new ItemStack(Material.GOLDEN_CARROT);
			goldcarrot.setAmount(1);
			loc.getWorld().dropItemNaturally(loc, goldcarrot);
			e.getPlayer().sendMessage(ChatColor.GOLD + "Your harvest has been blessed!");
		}
		
		// drop the items
		dropItems(loc, numDrops, type);
		
		// damage the tool
		damageToolInHand(e.getPlayer());
	}
	
	private void damageToolInHand(Player p) {
		// find the item in the player's hand
		ItemStack tool = p.getInventory().getItemInMainHand();
		// calculate unbreaking advantage
		int unbr = tool.getEnchantmentLevel(Enchantment.DURABILITY) + 1;
		// unbreaking advantage chance http://minecraft.gamepedia.com/Enchanting#Enchantments
		int chance = 100 / unbr;
		// if damage to item should randomly be applied
		if(utils.chance(chance)) {
			// take from durability
			p.getInventory().getItemInMainHand().setDurability((short) (tool.getDurability() + 1));
		}
	}
	
	private int getDropNumber(ItemStack toolitem, Material tool) {
		// initialize drops
		int drops;
		
	    switch(tool) {
	         case IRON_HOE:
	        	 drops = 2;
	        	 break;
	         case GOLD_HOE:
	             drops = 2;
	             break;
	         case DIAMOND_HOE:
	             drops = 3;
	             break;
	         default: // stone or wood
	             drops = 1;
	             break;
	       }
		
		// add 1 item for each Fortune enchantment level
		drops += toolitem.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
		
		return drops;
	}
	
	/** Drops quantity of harvest's appropriate items at loc.
	 * 
	 * @param loc Location to drop items at.
	 * @param quantity Quantity of items to be dropped.
	 * @param harvest The material type of the harvested block. */
	private void dropItems(Location loc, int quantity, Material harvest) {
		ItemStack crop;
		
		// determine what to drop
		switch(harvest) {
			case BEETROOT_BLOCK:
				crop = new ItemStack(Material.BEETROOT);
				break;
			case CROPS:
				crop = new ItemStack(Material.WHEAT);
				// special drop for seeds
				ItemStack seed = new ItemStack(Material.SEEDS);
				// set the quantity
				seed.setAmount(quantity);
				// drop the seed (crop dropped later)
				loc.getWorld().dropItemNaturally(loc, seed);
				break;
			case CARROT:
				crop = new ItemStack(Material.CARROT_ITEM);
				break;
			case NETHER_STALK:
				crop = new ItemStack(Material.NETHER_WARTS);
				break;
			default: // something went wrong, abort
				return;
		}
		
		// set the quantity
		crop.setAmount(quantity);
		
		// drop the crop
		loc.getWorld().dropItemNaturally(loc, crop);
	}
}
