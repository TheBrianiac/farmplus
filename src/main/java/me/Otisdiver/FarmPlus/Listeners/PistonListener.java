package me.Otisdiver.FarmPlus.Listeners;

import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;

import me.Otisdiver.FarmPlus.Utils;

/* Processes piston extension and retraction events. */
public class PistonListener implements Listener {
	
	// instance of utilities class
	private Utils utils;
	
	/** PistonListener processes piston extension and retraction events.
	 * 
	 * @param utils Instance of the Utilities class initialized in the main class. */
	public PistonListener(Utils utils) {
		this.utils = utils;
	}
	
	private void checkBlocks(List<Block> blocks) {
		// loop through blocks in list
		for(int i = 0; i < blocks.size(); i++) {
			Block block = blocks.get(i);
			// delete the block if necessary
			utils.checkBlock(block);
			// check for crops on soil and delete if necessary
			utils.checkSoil(block);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled=true)
	// runs when a piston is extended
	public void onExtend(BlockPistonExtendEvent e) {
		checkBlocks(e.getBlocks());
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled=true)
	// runs when a piston is retracted
	public void onRetract(BlockPistonRetractEvent e) {
		checkBlocks(e.getBlocks());
	}
	
}