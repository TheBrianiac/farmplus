package me.Otisdiver.FarmPlus.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/** Process water flow events. **/
import me.Otisdiver.FarmPlus.Utils;

/** Contains lightweight event handlers. **/
public class MiscListener implements Listener {
	
	// instance of utilities class
	private Utils utils;
	
	/** MiscListener contains lightweight event handlers.
	 * 
	 * @param utils Instance of the Utilities class initialized in the main class. */
	public MiscListener(Utils utils) {
		this.utils = utils;
	}
	
	// Event for when water flows to a block.
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onFlow(BlockFromToEvent e) {
		// delete the water/lava's destination block if necessary (cancels drops)
		utils.checkBlock(e.getToBlock());
	}
	
	// Event for when a block is destroyed by explosion.
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onExplode(BlockExplodeEvent e) {
		// delete the block to be exploded if necessary (cancels drops)
		utils.checkBlock(e.getBlock());
		// delete the block above the soil to be exploded if necessary
		utils.checkSoil(e.getBlock());
	}
	
	// Bukkit's superevent for player interaction. Includes crop trampling (what we're interested in).
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onInteract(PlayerInteractEvent e) {
		/*
		Physical action is described as "ass-pressure" by Bukkit JavaDocs.
		https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/event/block/Action.html#PHYSICAL
		If it isn't a physical action, we'll return (not go further).
		*/
		
		if(!e.getAction().equals(Action.PHYSICAL)) return;
		
		// check the soil for crops on it
		utils.checkSoil(e.getClickedBlock());
	}
}