package me.Otisdiver.FarmPlus;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.ChatColor;

public class Commander implements CommandExecutor {
	
	private String version;
	
	public Commander(String version) {
		this.version = version;
	}
	
	public boolean onCommand(CommandSender s, Command command, String cmd, String[] args) {
		if(cmd.equalsIgnoreCase("farmplus")) {
			s.sendMessage(ChatColor.GREEN + "Running FarmPlus v" + version);
			return true;
		}
		return false;
	}
	
}